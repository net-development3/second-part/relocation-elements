﻿using System;

namespace RelocationElementsTask
{
    public static class ArrayExtension
    {
        public static void MoveToTail(int[] source, int value)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Length == 0)
            {
                throw new ArgumentException(null, nameof(source));
            }

            int n = source.Length;

            for (int i = 0, count = 0; i < n; i++)
            {
                if (source[i] == value)
                {
                    count++;
                    for (int j = i; j < n - 1; j++)
                    {
                        source[j] = source[j + 1];
                    }

                    source[n - 1] = value;
                    n = source.Length - count;
                    i--;
                }
            }
        }
    }
}
